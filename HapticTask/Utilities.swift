//
//  Utilities.swift
//  HapticTask
//
//  Created by Darshan Shivakumar on 04/12/16.
//  Copyright © 2016 personal. All rights reserved.
//

import Foundation

//Custom cache class
class ImageCache {
    static let sharedCache: NSCache = { () -> NSCache<AnyObject, AnyObject> in
        let cache = NSCache<AnyObject, AnyObject>()
        cache.name = "ImageCache"
        cache.countLimit = 50
        cache.totalCostLimit = 30*1024*1024
        return cache
    }()
}
