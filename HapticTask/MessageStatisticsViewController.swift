//
//  MessageStatisticsViewController.swift
//  HapticTask
//
//  Created by Darshan Shivakumar on 04/12/16.
//  Copyright © 2016 personal. All rights reserved.
//

import UIKit
import CoreData

class MessageStatisticsViewController: UIViewController {

    @IBOutlet weak var statisticsTableView: UITableView!
    
    var users = [User]()
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let users = fetchObjects(){
            self.users = users
        }
        self.statisticsTableView.rowHeight = UITableView.automaticDimension
        self.statisticsTableView.estimatedRowHeight = 100.0
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.statisticsTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func fetchObjects() -> [User]?{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        if let result = try? self.context.fetch(fetchRequest) as? [User]{
            return result
        }else{
            return nil
        }
    }
}

extension MessageStatisticsViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = users[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.StatisticsCellIdentifier.STAT_CELL) as! MessageStatisticsCell
        
        cell.configureCell(withData: user)
        
        return cell
    }
}
