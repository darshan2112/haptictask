//
//  Protocols.swift
//  HapticTask
//
//  Created by Darshan Shivakumar on 03/12/16.
//  Copyright © 2016 personal. All rights reserved.
//

import Foundation

protocol DynamicCell {
//  For all cells dynaically populated
    func configureCell(withData data: AnyObject)
}
