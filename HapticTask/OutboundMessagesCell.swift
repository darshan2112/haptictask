//
//  OutboundMessagesCell.swift
//  HapticTask
//
//  Created by Darshan Shivakumar on 03/12/16.
//  Copyright © 2016 personal. All rights reserved.
//

import UIKit
import CoreData

class OutboundMessagesCell: UITableViewCell, DynamicCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var senderNameLabel: UILabel!
    
    @IBOutlet weak var timeStampLabel: UILabel!
    
    @IBOutlet weak var messageBodyLabel: UILabel!
    
    @IBOutlet weak var favoriteButton: UIButton!
    
    @IBAction func favoriteButtonTapped(_ sender: Any) {
        favoriteButtonTapped()
        
    }
    
    
    private var profileImageUrl : NSURL!
    
    private var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    private var isFavourite: Bool!
    private var time: String!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.width/2
        self.profileImageView.layer.masksToBounds = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.profileImageView.image = nil
    }
    
    func favoriteButtonTapped(){
        if self.isFavourite == true{
            self.isFavourite = false
            
        }else{
            self.isFavourite = true
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
        fetchRequest.predicate = NSPredicate(format: "time = %@", self.time)
        
        if let result = try? context.fetch(fetchRequest).first as? Message{
            result.isFavourite = self.isFavourite
        }
        do {
            try context.save()
        }catch let error{
            print("Error changing value of favorite.\n Error decription: \(error)")
        }
        self.configureFavouriteImageView()
    }
    
    func configureFavouriteImageView(){
        if self.isFavourite == true{
            DispatchQueue.main.async {
                self.favoriteButton.setImage(UIImage(named: "favourite.jpg"), for: UIControl.State.normal)
            }
            
        }else{
            DispatchQueue.main.async {
                self.favoriteButton.setImage(UIImage(named: "noFavourite.jpg"), for: UIControl.State.normal)
            }
        }
    }
    
    func configureCell(withData data: AnyObject) {
        let message = data as! Message
        
        self.messageBodyLabel.text = message.body
        self.timeStampLabel.text = trimTime(dateAndTime: message.time!)[1]
        self.senderNameLabel.text = message.sender?.name
        self.configureProfilePicForMessageItem(message: message)
        self.isFavourite = message.isFavourite
        self.time = message.time!
        configureFavouriteImageView()
    }
    
    private func configureProfilePicForMessageItem(message: Message){
        if message.sender?.profileimage != ""{
            
            self.profileImageUrl = NSURL(string: (message.sender?.profileimage)!)!
            if let image = NSURL(string: (message.sender?.profileimage)!)?.cachedImage{
                DispatchQueue.main.async {
                    self.profileImageView.image = image
                    self.profileImageView.alpha = 1
                }
                
            } else{
                
                self.profileImageView.alpha = 0
                
                NSURL(string:(message.sender?.profileimage)!)?.fetchImage(completion: { (image) in
                    if self.profileImageUrl == NSURL(string: (message.sender?.profileimage)!){
                        
                        DispatchQueue.main.async {
                            
                            self.profileImageView.image = image
                            
                        }
                            UIView.animate(withDuration: 0.3, animations: {
                                
                                self.profileImageView.alpha = 1
                                
                            })
                        
                    }
                })
                
            }
        }else{
            self.profileImageView.image = UIImage(named: "user.jpg")
        }
    }
    
    private func trimTime(dateAndTime: String) -> [String]{
        return dateAndTime.components(separatedBy: "T")
    }
    
    
}
