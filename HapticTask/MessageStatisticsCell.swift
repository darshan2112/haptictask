//
//  MessageStatisticsCell.swift
//  HapticTask
//
//  Created by Darshan Shivakumar on 04/12/16.
//  Copyright © 2016 personal. All rights reserved.
//

import UIKit

class MessageStatisticsCell: UITableViewCell, DynamicCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var totalMessageCountLabel: UILabel!
    
    @IBOutlet weak var favouriteMessageCountLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(withData data: AnyObject) {
        
        let user = data as! User
        
        self.nameLabel.text = user.name
        self.totalMessageCountLabel.text = String(describing: user.messages!.count)
        self.favouriteMessageCountLabel.text = String(describing: getFavouriteMessages(for: user))
        
    }
    
    func getFavouriteMessages(for user: User) -> Int{
        var favCount = 0
        let messages = Array(user.messages!) as! [Message]
        for message in messages{
            if message.isFavourite == true{
                favCount += 1
            }
        }
        return favCount
    }

}
