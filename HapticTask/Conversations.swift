//
//  Conversations.swift
//  HapticTask
//
//  Created by Darshan Shivakumar on 03/12/16.
//  Copyright © 2016 personal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreData

class MessageObject: NSObject{

    var sender : UserObject
    var time: String
    var body: String
    var isFavourite: Bool
    
    init (sender: UserObject, time: String, body: String, isFavourite: Bool){
        self.sender = sender
        self.time = time
        self.body = body
        self.isFavourite = isFavourite
    }
}

class UserObject : NSObject{
    var name: String
    var userName: String
    var profileImage: String
    
    init(name: String, userName: String, profileImage: String){
        self.name = name
        self.userName = userName
        self.profileImage = profileImage
    }
}








