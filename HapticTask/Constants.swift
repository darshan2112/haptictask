//
//  Constants.swift
//  HapticTask
//
//  Created by Darshan Shivakumar on 03/12/16.
//  Copyright © 2016 personal. All rights reserved.
//

import Foundation

struct Constants {
    
    struct MessageCellIdentifier{
        static let INBOUND_CELL = "MessageInboundCell"
        static let OUTBOUND_CELL = "MessageOutboundCell"
    }
    
    struct  StatisticsCellIdentifier {
        static let STAT_CELL = "StatisticsCell"
    }
    
    struct MessageResponse{
        static let MESSAGES = "messages"
        static let BODY = "body"
        static let USERNAME = "username"
        static let IMAGE_URL = "image-url"
        static let MESSAGE_TIME = "message-time"
        static let NAME = "Name"
    }
    
    static let BASE_URL =  "http://haptik.co/app/"
    
    
    static let TEST_DATA = "test_data/"
    
    
    static let CONVERSATION_REQUEST = Constants.BASE_URL + Constants.TEST_DATA
    
    
}
