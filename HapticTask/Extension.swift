//
//  Extension.swift
//  HapticTask
//
//  Created by Darshan Shivakumar on 03/12/16.
//  Copyright © 2016 personal. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension NSURL{
    typealias ImageCacheCompletion = (UIImage) -> Void
    
    // Retrieves a pre-cached image, or nil if it isn't cached.
    var cachedImage: UIImage? {
        return ImageCache.sharedCache.object(
            forKey: absoluteString as AnyObject) as? UIImage
    }
    
    // Fetches the image from the network.
    func fetchImage(completion: @escaping ImageCacheCompletion) {
        let task = URLSession.shared.dataTask(with: self as URL) {
            data, response, error in
            if error == nil {
                if let  data = data,
                    let image = UIImage(data: data) {
                    ImageCache.sharedCache.setObject(
                        image,
                        forKey: self.absoluteString as AnyObject,
                        cost: data.count)
                    DispatchQueue.main.async {
                        completion(image)
                    }
                }
            }
        }
        task.resume()
    }
}


extension Message{
    class func messageWithMessageInfo(messageinfo: MessageObject, inManagedObjectContext context: NSManagedObjectContext) -> Message?{
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
        request.predicate = NSPredicate(format: "time = %@", messageinfo.time)
        
        if let message = (try? context.fetch(request))?.first as? Message{
            return message
        }else if let message = NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as? Message{
            
            message.body = messageinfo.body
            message.isFavourite = messageinfo.isFavourite
            message.time = messageinfo.time
            message.sender = User.senderWithUserInfo(userInfo: messageinfo.sender, inManagedObjectContext: context)
            
            return message
        }
        
        return nil
    }
}

extension User{
    class func senderWithUserInfo(userInfo: UserObject, inManagedObjectContext context: NSManagedObjectContext) -> User?{
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.predicate = NSPredicate(format: "username = %@", userInfo.userName)
        
        if let user = (try? context.fetch(request))?.first as? User{
            return user
        }else if let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: context) as? User{
            user.username = userInfo.userName
            user.name = userInfo.name
            user.profileimage = userInfo.profileImage
            return user
        }
        
        return nil
    }
    
}
