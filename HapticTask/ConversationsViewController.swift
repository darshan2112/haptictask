//
//  ConversationsViewController.swift
//  HapticTask
//
//  Created by Darshan Shivakumar on 03/12/16.
//  Copyright © 2016 personal. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

class ConversationsViewController: UIViewController {
    
    @IBOutlet weak var conversationTableView: UITableView!
    
    var context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    var mutableList = NSMutableOrderedSet()
    
    var messages = [MessageObject]()
    var user = [UserObject]()
    var messageList = [Message]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let messageList = self.fetchObjects(){
            self.messageList = messageList
        }
        self.conversationTableView.rowHeight = UITableView.automaticDimension
        self.conversationTableView.estimatedRowHeight = 100.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllMessages(requestUrl: Constants.CONVERSATION_REQUEST)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.conversationTableView.reloadData()
    }

    func getAllMessages(requestUrl: String){
        
        Alamofire.request(requestUrl).responseJSON { (response) in
            
            switch response.result{
                
            case .success(let data):
                
                let responseData = JSON(data)
                var time: String
                var body: String
                var isFavourite: Bool
                var name : String
                var userName: String
                var profileImage: String
                
                var message : MessageObject
                var user : UserObject
                
                for i in 0..<responseData["messages"].count{
                    
                    name = responseData[Constants.MessageResponse.MESSAGES][i][Constants.MessageResponse.NAME].stringValue
                    body = responseData[Constants.MessageResponse.MESSAGES][i][Constants.MessageResponse.BODY].stringValue
                    profileImage = responseData[Constants.MessageResponse.MESSAGES][i][Constants.MessageResponse.IMAGE_URL].stringValue
                    time = responseData[Constants.MessageResponse.MESSAGES][i][Constants.MessageResponse.MESSAGE_TIME].stringValue
                    userName = responseData[Constants.MessageResponse.MESSAGES][i][Constants.MessageResponse.USERNAME].stringValue
                    isFavourite = false
                    
                    user = UserObject(name: name, userName: userName, profileImage: profileImage)
                    
                    message = MessageObject(sender: user, time: time, body: body, isFavourite: isFavourite)
                    
                    self.messages.append(message)
                    self.user.append(user)
                }
                
                self.updateDatabase(newMessages: self.messages)
                
            case .failure(let error):
                print("Request failed with error. Error Description: \(error)")
                
            }
        }
        
    }
    
    func updateDatabase(newMessages: [MessageObject]){
        for messageInfo in newMessages{
            _ = Message.messageWithMessageInfo(messageinfo: messageInfo, inManagedObjectContext: self.context!)
        }
        
        do{
            try context!.save()
            print("Successfully added")
            self.messageList = self.fetchObjects()!
            self.conversationTableView.reloadData()
        }catch let error{
            print("Error saving new messages. Error Description: \(error)")
        }
    }
    
    
    
    func fetchObjects() -> [Message]?{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "time", ascending: true)]
        
        if let result = try? self.context!.fetch(fetchRequest) as? [Message]{
            return result
        }else{
            return nil
        }
    }    
}

extension ConversationsViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messageList[indexPath.row]
        if message.sender?.profileimage == ""{
        
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.MessageCellIdentifier.OUTBOUND_CELL, for: indexPath) as! OutboundMessagesCell
        
            cell.configureCell(withData: message)
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.MessageCellIdentifier.INBOUND_CELL, for: indexPath) as! InboundmessagesCell
            
            cell.configureCell(withData: message)
            
            return cell
        }
    }
}

extension ConversationsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
